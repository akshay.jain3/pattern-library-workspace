import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core'

/**
 * Input field component
 */
@Component({
  selector: 'app-input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.scss'],
})
export class InputFieldComponent implements OnInit {
  /** input property */
  @Input() value = ''
  /**
   * label
   */
  @Input() label = 'label'
  /**
   * text
   */
  @Input() type = 'text'
  /**
   * placeholder
   */
  @Input() placeholder = ''
  /**
   * required
   */
  @Input() required = false
  /**
   * success state
   */
  @Input() success = false
  /**
   * error state
   */
  @Input() error = false
  /**
   * showHelperText
   */
  @Input() showHelperText = false
  /**
   * helperText
   */
  @Input() helperText = 'helper text'
  /**
   * getFieldValue
   */
  @Output() getFieldValue = new EventEmitter<string>()
  /**
   * getFieldValueOnBlur
   */
  @Output() getFieldValueOnBlur = new EventEmitter<string>()
  /**
   * maxLength
   */
  @Input() maxLength : number
  /**
   * disabled
   */
  @Input() disabledInput = false
   /**
    * input id
    */
  @Input() inputId: string

  constructor() {}

  /**
   * This method runs when component initialized first time.
   */
  ngOnInit() {}

  /**
   * This method runs to emit value on keyup
   */
  checkValueOnKeyUp(event) {
    this.getFieldValue.emit(event.target.value)
  }

  /**
   * This method runs to emit value on blur
   */
  checkValueOnBlur(event) {
    this.getFieldValueOnBlur.emit(event.target.value)
  }

  /**
   * This method runs to check maximu lenght of field
   */
  checkMaxLength(event): boolean | undefined {
    if(event.target.value.length + 1 > this.maxLength) {
      return false
    }
  }
}
