import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputFieldComponent } from './input-field.component';

describe('InputFieldComponent', () => {
  let component: InputFieldComponent;
  let fixture: ComponentFixture<InputFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('checkValueOnKeyUp is working fine', () => {
    const event ={target:{
      value:'asdq'
    }}
    component.checkValueOnKeyUp(event)
    expect(component).toBeTruthy();
  });

  it('checkValueOnBlur is working fine', () => {
    const event ={target:{
      value:'asdw'
    }}
    component.checkValueOnBlur(event)
    expect(component).toBeTruthy();
  });

  it('checkMaxLength is working fine', () => {
    const event ={target:{
      value:'asdd'
    }}
    component.checkMaxLength(event)
    expect(component).toBeTruthy();
  });

 
});
