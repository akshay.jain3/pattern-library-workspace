import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { InplaceLoaderComponent } from './inplace-loader.component'
import { ProgressSpinner } from 'primeng'

describe('InplaceLoaderComponent', () => {
  let component: InplaceLoaderComponent
  let fixture: ComponentFixture<InplaceLoaderComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InplaceLoaderComponent, ProgressSpinner],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(InplaceLoaderComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
