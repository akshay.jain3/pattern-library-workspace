import { Component, Input, OnInit } from '@angular/core'

/** Inplace loader component styles */
@Component({
  selector: 'app-inplace-loader',
  templateUrl: './inplace-loader.component.html',
  styleUrls: ['./inplace-loader.component.scss'],
})
export class InplaceLoaderComponent implements OnInit {
  /** show */
  @Input() show = false
  /** customStyles */
  @Input() customStyles: any

  constructor() {}

  /** This method runs when component initialized first time */
  ngOnInit() {}
}
