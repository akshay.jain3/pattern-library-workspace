import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { EditorModule } from '@tinymce/tinymce-angular'

import { CustomEditorComponent } from './custom-editor.component'
import { CharacterCountComponent } from 'components/common/character-count/character-count.component'
import { NO_ERRORS_SCHEMA } from '@angular/core'

describe('CustomEditorComponent', () => {
  let component: CustomEditorComponent
  let fixture: ComponentFixture<CustomEditorComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustomEditorComponent, CharacterCountComponent],
      imports: [FormsModule, ReactiveFormsModule, EditorModule],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomEditorComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
