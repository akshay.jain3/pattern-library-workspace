import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { FormBuilder } from '@angular/forms'

declare var tinymce: any

/** cast html element to event target */
interface HTMLInputEvent extends Event {
  /** event target */
  target: HTMLInputElement & EventTarget
}

/** Tags */
export const TAGS = [
  { text: 'Appointment Date', tagValue: '{apptdate}', value: 'apptdate', keyCount:13 },
  { text: 'Arrival Time', tagValue: '{arrivaltime}', value: 'arrivaltime', keyCount:7 },
  { text: 'Caller Id', tagValue: '{callerid}', value: 'callerid', keyCount:12 },
  { text: 'Location', tagValue: '{location}', value: 'location', keyCount:24 },
  { text: 'Patient', tagValue: '{patient}', value: 'patient', keyCount:20 },
  { text: 'Phonetic Address', tagValue: '{phoneticaddress}', value: 'phoneticaddress', keyCount:36 },
  { text: 'Provider', tagValue: '{provider}', value: 'provider', keyCount:20 },
]

/** Custom editor component */
@Component({
  selector: 'app-custom-editor',
  templateUrl: './custom-editor.component.html',
  styleUrls: ['./custom-editor.component.scss'],
})
export class CustomEditorComponent implements OnInit {
  /** editorInit */
  editorInit: any
  /** focus */
  focus = false
  /** editorReference */
  editorReference: any
  /** input */
  input: any
  /** meta */
  meta: any
  /** file */
  file: any
  /** reader */
  reader: any
  /** id */
  id: any
  /** blobCache */
  blobCache: any
  /** base64 */
  base64: any
  /** blobInfo */
  blobInfo: any
  /** tags */
  @Input() tags = TAGS
  /** success */
  @Input() success = false
  /** error */
  @Input() error = false
  /** label */
  @Input() label: string
  /** required */
  @Input() required = false
  /** helperText */
  @Input() helperText = ''
  /** limitCountLength */
  @Input() limitCountLength: number
  /** maxCountLength */
  @Input() maxCountLength: number
  /** currentCountLength */
  @Input() currentCountLength: number
  /** initialValue */
  @Input() initialValue = ''
  /** character count required flag */
  @Input() characterCountRequired = false
  /** height */
  @Input() height = 200
  /** editorPlugins */
  @Input() editorPlugins = ''
  /** editorToolbar */
  @Input() editorToolbar = ''
  /** handleOnChange */
  @Output() handleOnChange = new EventEmitter<any>()
  /** handleOnBlur */
  @Output() handleOnBlur = new EventEmitter<any>()
  /** handleOnFocus */
  @Output() handleOnFocus = new EventEmitter<any>()
  /** handleOnKeyup */
  @Output() handleOnKeyup = new EventEmitter<any>()
  /** validateImageSize */
  @Output() validateImageSize = new EventEmitter<any>()

  constructor(private readonly fb: FormBuilder) {}

  /** This method runs when component initialized first time. */
  ngOnInit() {
    this.textEditorInit()
  }

  /** textEditorInit */
  textEditorInit() {
    this.editorInit = {
      base_url: '/tinymce',
      suffix: '.min',
      height: this.height,
      menubar: false,
      statusbar: false,
      image_description: false,
      plugins: `noneditable ${this.editorPlugins}`,
      toolbar: `${this.editorToolbar}`,
      entity_encoding: 'raw',
      noneditable_noneditable_class: 'tag',
      file_picker_types: 'image',
      // here's our custom image picker
      file_picker_callback: (cb, value, meta) => {
        this.input = document.createElement('input')
        this.input.setAttribute('type', 'file')
        this.input.setAttribute('accept', 'image/jpg, image/jpeg, image/png, image/gif')
        this.meta = meta

        this.input.onchange = (e?: HTMLInputEvent) => {
          this.file = e.target.files[0]
          this.reader = new FileReader()
          this.reader.onload = () => {
            this.id = 'blobid' + new Date().getTime()
            this.blobCache = tinymce.activeEditor.editorUpload.blobCache
            this.base64 = (this.reader.result as string).split(',')[1]
            this.blobInfo = this.blobCache.create(this.id, this.file, this.base64)
            this.blobCache.add(this.blobInfo)

            this.sendImageSizeToComponent(this.file.size)

            cb(this.blobInfo.blobUri(), { title: this.file.name })
          }
          this.reader.readAsDataURL(this.file)
        }

        this.input.click()
      },
      setup: editor => {
        editor.ui.registry.addMenuButton('tags', {
          text: '{ Tags }',
          tooltip: 'Insert tags',
          fetch: callback => {
            const items = this.tags.map(tag => {
              return {
                type: 'menuitem',
                text: `${tag.text} (${tag.keyCount})`,
                onAction: () => {
                  editor.insertContent(` <span class="tag">${tag.tagValue}</span> `)
                },
              }
            })
            callback(items)
          },
        })
        editor.on('init', () => {
          this.editorReference = editor
          editor.setContent(this.initialValue)
        }),
          editor.on('NodeChange', (e, parents, selectionChange) => {
            if (e.element.tagName === 'IMG') {
              const pieces = e.element.currentSrc.split(/[\s/]+/)
              e.element.setAttribute('alt', pieces[pieces.length - 1])
            }
          })
        editor.on('keyup', () => {
          this.handleOnChange.emit(editor.getContent())
          this.handleOnKeyup.emit(editor.getContent())
        }),
          editor.on('change', () => {
            this.handleOnChange.emit(editor.getContent())
          }),
          editor.on('blur', () => {
            this.focus = false
            this.handleOnBlur.emit(editor.getContent())
          }),
          editor.on('focus', () => {
            this.focus = true
            this.handleOnFocus.emit(editor.getContent())
          })
      },
      content_style: `
                .tag {
                    background-color: #D6F0FF;
                    padding: 1px 3px;
                    color: #44719B;
                    font-family: Lato, sans-serif;
                    font-size: 16px;
                    margin: 0 2px;
                    font-weight: normal;
                    font-style: normal;
                }
            `,
    }
  }

  /** getClasses */
  getClasses() {
    return {
      focus: this.focus,
      error: this.error,
      success: this.success,
    }
  }

  /** setEditorValueOnInit */
  setEditorValueOnInit(value) {
    this.initialValue = value
    if (this.editorReference) {
      this.editorReference.setContent(this.initialValue)
    }
  }

  /** sendImageSizeToComponent to subscribe and callback. */
  sendImageSizeToComponent(imageSize) {
    this.validateImageSize.emit(imageSize)
  }
}
