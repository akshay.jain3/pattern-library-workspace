import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { CheckboxWithLabelComponent } from './checkbox-with-label.component'
import { NO_ERRORS_SCHEMA } from '@angular/core'
import { CommentStmt } from '@angular/compiler'

describe('CheckboxWithLabelComponent', () => {
  let component: CheckboxWithLabelComponent
  let fixture: ComponentFixture<CheckboxWithLabelComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CheckboxWithLabelComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxWithLabelComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('checkboxClicked is working fine', () => {
    component.isDisabled = false
    fixture.detectChanges()
    component.checkboxClicked()
    expect(component).toBeTruthy()
  })
})
