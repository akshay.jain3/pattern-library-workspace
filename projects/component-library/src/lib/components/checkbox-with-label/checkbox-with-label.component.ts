import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'

/**
 * Checkbox with label component
 */
@Component({
  selector: 'app-checkbox-with-label',
  templateUrl: './checkbox-with-label.component.html',
  styleUrls: ['./checkbox-with-label.component.scss'],
})
export class CheckboxWithLabelComponent implements OnInit {
  /**
   * isChecked
   */
  @Input() isChecked = false
  /**
   * label
   */
  @Input() label = 'label'
  /**
   * show customIcon
   */
  @Input() customIcon = false
  /**
   * isDisabled
   */
  @Input() isDisabled = false
  /**
   * icon
   */
  @Input() icon = ''
  /**
   * isChecked
   */
  @Output() isCheckedData: EventEmitter<any> = new EventEmitter()

  constructor() {}

  /**
   * This method runs when component initialized first time.
   */
  ngOnInit() {}

  /**
   * Called on click of checkbox
   */
  checkboxClicked() {
    if(!this.isDisabled) {
      this.isChecked = !this.isChecked
      this.isCheckedData.emit(this.isChecked)
    }
  }
}
