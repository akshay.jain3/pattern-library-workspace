import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { DropdownComponent } from './dropdown.component'
import { DropdownModule } from 'primeng'
import { FormsModule } from '@angular/forms'
import { NO_ERRORS_SCHEMA } from '@angular/core'

describe('DropdownComponent', () => {
  let component: DropdownComponent
  let fixture: ComponentFixture<DropdownComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DropdownComponent],
      imports: [FormsModule, DropdownModule],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('onChangeData is working fine', () => {
    const event ={}
    component.onChangeData(event)
    expect(component).toBeTruthy()
  })
})
