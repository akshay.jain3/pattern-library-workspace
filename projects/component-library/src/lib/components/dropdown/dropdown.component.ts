import { Component, EventEmitter, Input, OnInit, Output, ChangeDetectorRef, ViewRef, ViewChild } from '@angular/core'

/**
 * Dropdown component
 */
@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
})
export class DropdownComponent implements OnInit {
  /** dropdown items */
  @Input() items: any[] = []
  /** selected item */
  @Input() selectedItem: any
  /** isDisabled */
  @Input() isDisabled = false
  /** label */
  @Input() label = 'label'
  /** secondaryLabel */
  @Input() secondaryLabel = ''
  /** label */
  @Input() placeholderText = ''
  /** showClear */
  @Input() showClear = false
  /** preselected item */
  @Input() selected: number
  /** isGrouped */
  @Input() isGrouped = false
  /** showIcons */
  @Input() showIcons = false
  /** customStyles */
  @Input() customStyles: any
  /** iconClass */
  @Input() iconClass = ''
  /** iconClass */
  @Input() imageUrl = ''
  /** optionLabel */
  @Input() optionLabel = 'label'
  /** helperText */
  @Input() helperText = ''
  /** showCustomClear */
  @Input() showCustomClear = false
  /** showRemove */
  @Input() showRemove = false
  /** enableVirtualScroll */
  @Input() enableVirtualScroll = false
  /** itemSize */
  @Input() itemSize: number
  /** handleOnChange */
  @Output() handleOnChange = new EventEmitter<number>()
  /** clearClick */
  @Output() clearClick = new EventEmitter<any>()
  /** removeItemClick */
  @Output() removeItemClick = new EventEmitter<any>()
  /** dropdownRef */
  @ViewChild('dropdown', { static: false }) dropdownRef: any

  constructor(private readonly changeDetectorRef: ChangeDetectorRef) {}

  /** initial function */
  ngOnInit() { }

  /**
   * emit event on change data
   * @param event event object
   */
  onChangeData(event) {
    this.handleOnChange.emit(this.selectedItem)
    if (!(this.changeDetectorRef as ViewRef).destroyed) {
      this.changeDetectorRef.detectChanges()
    }
  }

  /** handleClearClick */
  handleClearClick() {
    this.clearClick.emit(() => {
      this.dropdownRef.clear()
    })
  }

  /** removeItemClick */
  removeItemClickFn(event, item) {
    event.stopPropagation()
    this.removeItemClick.emit(item)
  }
}
