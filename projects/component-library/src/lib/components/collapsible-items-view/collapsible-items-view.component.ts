import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { faChevronDown, faChevronUp } from '@fortawesome/free-solid-svg-icons'
// import clearIcon from './clear-icon.svg'

/**
 * Collapsible items view component
 */
@Component({
  selector: 'app-collapsible-items-view',
  templateUrl: './collapsible-items-view.component.html',
  styleUrls: ['./collapsible-items-view.component.scss'],
})
export class CollapsibleItemsViewComponent implements OnInit {
  /**
   * Accordion is Expanded or not
   */
  @Input() isExpanded = true //TODO: change later
  /**
   * Fontawesome up chevron icon
   */
  faChevronUp = faChevronUp
  /**
   * Fontawesome down chevron icon
   */
  faChevronDown = faChevronDown
  /**
   * primary label
   */
  @Input() primaryLabel = 'primary label'
  /**
   * icon class
   */
  @Input() iconClass = ''
  /**
   * list items
   */
  @Input() listItems: any[]
  /**
   * clear icon
   */
  // clearIcon = clearIcon
  /**
   * show
   */
  @Input() show = false
  /**
   * removeAllItems
   */
  @Output() removeAllItems = new EventEmitter<any>()
  /**
   * removeItem
   */
  @Output() removeItem = new EventEmitter<any>()
  /**
   * expanded
   */
  @Output() expanded = new EventEmitter<any>()

  constructor() {}

  /**
   * This method runs when component initialized first time.
   */
  ngOnInit() {}

  /**
   * This method runs when a chevron is clicked, and toggles the appropriate accordion.
   */
  collapseToggle() {
    this.isExpanded = !this.isExpanded
    this.expanded.emit(this.isExpanded)
  }

  /**
   * removeAllItems
   */
  removeAllItemsFn(itemType) {
    this.removeAllItems.emit({ itemType })
  }

  /**
   * removeItem
   * @param item - item to be removed
   * @param itemType - itemType
   */
  removeItemFn(item, itemType) {
    this.removeItem.emit({ item, itemType })
  }
}
