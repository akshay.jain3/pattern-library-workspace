import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { CollapsibleItemsViewComponent } from './collapsible-items-view.component'
import { FaIconComponent } from '@fortawesome/angular-fontawesome'
import { ButtonComponent } from '../button/button.component'
import { NO_ERRORS_SCHEMA } from '@angular/core'

describe('CollapsibleItemsViewComponent', () => {
  let component: CollapsibleItemsViewComponent
  let fixture: ComponentFixture<CollapsibleItemsViewComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CollapsibleItemsViewComponent,
        FaIconComponent,
        ButtonComponent,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(CollapsibleItemsViewComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('collapseToggle is working fine', () => {
    component.collapseToggle()
    expect(component).toBeTruthy()
  })





  it('removeAllItemsFn is working fine', () => {
    const itemType =[]
    component.removeAllItemsFn(itemType)
    expect(component).toBeTruthy()
  })

  it('removeItemFn is working fine', () => {
    const itemType = 'Providers'
    const item = []
    component.removeItemFn(item,itemType)
    expect(component).toBeTruthy()
  })


})
