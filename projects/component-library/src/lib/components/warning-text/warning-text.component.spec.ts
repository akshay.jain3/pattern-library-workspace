import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { WarningTextComponent } from './warning-text.component'

describe('WarningTextComponent', () => {
  let component: WarningTextComponent
  let fixture: ComponentFixture<WarningTextComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WarningTextComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(WarningTextComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
