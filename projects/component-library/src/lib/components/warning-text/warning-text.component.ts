import { Component, Input, OnInit } from '@angular/core'

/**
 * Warning text component
 */
@Component({
  selector: 'app-warning-text',
  templateUrl: './warning-text.component.html',
  styleUrls: ['./warning-text.component.scss'],
})
export class WarningTextComponent implements OnInit {
  /** warning text */
  @Input() text = 'warning text placeholder'

  constructor() {}

  /** This method runs when component initialized first time. */
  ngOnInit() {}
}
