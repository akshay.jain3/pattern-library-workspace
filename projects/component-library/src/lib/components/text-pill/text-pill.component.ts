import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core'

/**
 * Text pill component
 */
@Component({
  selector: 'app-text-pill',
  templateUrl: './text-pill.component.html',
  styleUrls: ['./text-pill.component.scss'],
})
export class TextPillComponent implements OnInit {
  /**
   * uniqueId
   */
  @Input() uniqueId: bigint
  /**
   * name
   */
  @Input() name: string
  /**
   * showErrorState
   */
  @Input() showErrorState = false
  /**
   * name
   */
  @Output() remove = new EventEmitter()
  /**
   * success state property
   */
  @Input() showSuccessState = false

  constructor() {}

  /**
   * This method runs when component initialized first time.
   */
  ngOnInit() {}

  /**
   * removeFn
   * @param event - event
   * @param uniqueId - uniqueId
   */
  removeFn(event, uniqueId) {
    if (event) {
      event.stopPropagation();
    }
    this.remove.emit(uniqueId)
  }
}
