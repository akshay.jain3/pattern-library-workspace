import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { TextPillComponent } from './text-pill.component'
import { NO_ERRORS_SCHEMA } from '@angular/core'

describe('TextPillComponent', () => {
  let component: TextPillComponent
  let fixture: ComponentFixture<TextPillComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TextPillComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(TextPillComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('removeFn is working fine', () => {
    spyOn(component.remove, 'emit')
    component.removeFn('',1)
    expect(component.remove.emit).toHaveBeenCalledWith(1)
  })
})
