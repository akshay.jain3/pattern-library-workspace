import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'

/**
 * Button component
 */
@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent implements OnInit {
  /**
   * Icon class
   */
  @Input() iconClass = ''
  /**
   * Icon image
   */
  @Input() iconImage = ''
  /**
   * Button text
   */
  @Input() btnText = 'btn text'
  /**
   * Loading text
   */
  @Input() loadingText = 'loading...'
  /**
   * Button theme
   */
  @Input() theme = 'primary'
  /**
   * isLoading
   */
  @Input() isLoading = false
  /**
   * buttonClick
   */
  @Output() buttonClick = new EventEmitter<any>()
  /**
   * disabled
   */
  @Input() disabled = false 

  constructor() {}

  /**
   * This method runs when component initialized first time.
   */
  ngOnInit() {}

  /**
   * onButtonClick
   * @param event - event object
   */
  onButtonClick(event) {
    this.buttonClick.emit(event)
  }
}
