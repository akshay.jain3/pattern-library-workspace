import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { AccordionComponent } from './accordion.component'
import { FaIconComponent } from '@fortawesome/angular-fontawesome'

describe('AccordionComponent', () => {
  let component: AccordionComponent
  let fixture: ComponentFixture<AccordionComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccordionComponent, FaIconComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(AccordionComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  

  it('collapseToggle is working fine', () => {
    component.collapseToggle()
    expect(component).toBeTruthy()
  })
})
