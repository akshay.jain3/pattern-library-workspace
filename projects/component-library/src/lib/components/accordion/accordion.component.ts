import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core'
import { faChevronDown, faChevronUp } from '@fortawesome/free-solid-svg-icons'

/**
 * Accordion component
 */
@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss'],
})
export class AccordionComponent implements OnInit {
  /**
   * Accordion is Expanded or not
   */
  @Input() isExpanded = false
  /**
   * accordion body template
   */
  @Input() accordionBody: TemplateRef<any>
  /**
   * Fontawesome up chevron icon
   */
  faChevronUp = faChevronUp
  /**
   * Fontawesome down chevron icon
   */
  faChevronDown = faChevronDown
  /**
   * primary label
   */
  @Input() primaryLabel = 'primary label'
  /**
   * show secondary label
   */
  @Input() showSecondaryLabel = false
  /**
   * secondary label
   */
  @Input() secondaryLabel = ''
  /**
   * theme class
   */
  @Input() theme = 'primary'
  /**
   * getExpandedState
   */
  @Output() getExpandedState = new EventEmitter()
  /**
   * showCheckbox
   */
  @Input() showCheckbox = false

  constructor() {}

  /**
   * This method runs when component initialized first time.
   */
  ngOnInit() {}

  /**
   * This method runs when a chevron is clicked, and toggles the appropriate accordion.
   */
  collapseToggle() {
    this.isExpanded = !this.isExpanded
    this.getExpandedState.emit(this.isExpanded)
  }
}
