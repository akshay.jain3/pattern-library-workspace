import { Component, Input, OnInit } from '@angular/core'

/**
 * Character count component
 */
@Component({
  selector: 'app-character-count',
  templateUrl: './character-count.component.html',
  styleUrls: ['./character-count.component.scss'],
})
export class CharacterCountComponent implements OnInit {
  /** currentCount */
  @Input() currentCount: number
  /** limit */
  @Input() limit: number
  /** maxCount */
  @Input() maxCount: number
  /** helperText */
  @Input() helperText = '(Recommended 160 characters or less)'

  constructor() {}

  /** This method runs when component initialized first time. */
  ngOnInit() { }
}
