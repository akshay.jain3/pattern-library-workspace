import { Component, Input, OnInit } from '@angular/core'

/**
 * Info component
 */
@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
})
export class InfoComponent implements OnInit {
  /** text */
  @Input() text = 'info title'
  /** description */
  @Input() description = ''
  /** showInfo */
  @Input() showInfo = false
  /** showCloseIcon */
  @Input() showCloseIcon = true

  constructor() {}

  /** This method runs when component initialized first time. */
  ngOnInit() {}

  /**
   * handleClick
   * @param event - event
   */
  handleClick(event){
    this.showInfo = !this.showInfo
  }

  /** closeInfo */
  closeInfo() {
    this.showInfo = false
  }
}
