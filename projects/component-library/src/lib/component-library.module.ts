import { NgModule } from '@angular/core';
// import {AccordionComponent} from './components/accordion/accordion.component';
import {ButtonComponent} from './components/button/button.component';
import {CharacterCountComponent} from './components/character-count/character-count.component';
// import {DropdownComponent} from './components/dropdown/dropdown.component';
import {InfoComponent} from './components/info/info.component';
import {CommonModule} from '@angular/common';
import {CheckboxWithLabelComponent} from './components/checkbox-with-label/checkbox-with-label.component';
import {AccordionComponent} from './components/accordion/accordion.component';
import {CollapsibleItemsViewComponent} from './components/collapsible-items-view/collapsible-items-view.component';
import {CustomEditorComponent} from './components/custom-editor/custom-editor.component';
import {DropdownComponent} from './components/dropdown/dropdown.component';
import {InplaceLoaderComponent} from './components/inplace-loader/inplace-loader.component';
import {InputFieldComponent} from './components/input-field/input-field.component';
import {TextPillComponent} from './components/text-pill/text-pill.component';
import {WarningTextComponent} from './components/warning-text/warning-text.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { EditorModule as TinyEditorModule, TINYMCE_SCRIPT_SRC } from '@tinymce/tinymce-angular'
import {DropdownModule, ProgressSpinnerModule} from 'primeng';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AccordionComponent,
    ButtonComponent,
    CharacterCountComponent,
    CheckboxWithLabelComponent,
    CollapsibleItemsViewComponent,
    CustomEditorComponent,
    DropdownComponent,
    InplaceLoaderComponent,
    InputFieldComponent,
    TextPillComponent,
    WarningTextComponent,
    InfoComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    TinyEditorModule,
    DropdownModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ProgressSpinnerModule,
  ],
  exports: [
    AccordionComponent,
    ButtonComponent,
    CharacterCountComponent,
    CheckboxWithLabelComponent,
    CollapsibleItemsViewComponent,
    CustomEditorComponent,
    DropdownComponent,
    InplaceLoaderComponent,
    InputFieldComponent,
    TextPillComponent,
    WarningTextComponent,
    InfoComponent
  ],
  providers: [
    {provide: TINYMCE_SCRIPT_SRC, useValue: 'tinymce/tinymce.min.js'}
  ]
})
export class ComponentLibraryModule { }
