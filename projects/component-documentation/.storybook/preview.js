import { addParameters } from '@storybook/angular';
import { DocsContainer, DocsPage } from "@storybook/addon-docs/dist/blocks";

export const MINIMAL_VIEWPORTS = {
  xMobile: {
    name: 'xMobile',
    styles: {
      height: '568px',
      width: '480px',
    },
    type: 'mobile',
  },
  mobile: {
    name: 'mobile',
    styles: {
      height: '896px',
      width: '768px',
    },
    type: 'mobile',
  },
  xTablet: {
    name: 'xTablet',
    styles: {
      height: '1112px',
      width: '1024px',
    },
    type: 'tablet',
  },
  tablet: {
    name: 'tablet',
    styles: {
      height: '1112px',
      width: '1200px',
    },
    type: 'tablet',
  },
  xDesktop: {
    name: 'xDesktop',
    styles: {
      height: '1112px',
      width: '1408px',
    },
    type: 'large screen',
  },
  desktop: {
    name: 'desktop',
    styles: {
      height: '1112px',
      width: '1920px',
    },
    type: 'large screen',
  },
};

addParameters({
  notes: 'Default Notes...',
  viewport: {
    viewports: MINIMAL_VIEWPORTS, // newViewports would be an ViewportMap. (see below for examples)
    defaultViewport: MINIMAL_VIEWPORTS.xMobile.name,
  },
  docs: {
    container: DocsContainer,
    page: DocsPage
  },
  passArgsFirst: false
});
