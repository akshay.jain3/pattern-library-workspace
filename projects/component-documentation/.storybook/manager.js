import { addons } from '@storybook/addons';
import { create } from '@storybook/theming/create';

addons.setConfig({
  theme: create({
    base: 'light',
    brandTitle: 'Relatient - Pattern Library',
    brandImage: 'https://jira.relatient.net/s/whhlrm/804002/c7d8e6a3d58a77726b1eb6218604b9ff/_/jira-logo-scaled.png'
  }),
});
