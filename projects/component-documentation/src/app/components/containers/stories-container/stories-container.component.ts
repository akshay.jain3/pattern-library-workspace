import {Component, Input, OnInit, TemplateRef} from '@angular/core';

/**
 * Stories container component:- Use this to wrap the components inside the bootstrap container with margin.
 */
@Component({
  selector: 'app-stories-container',
  templateUrl: './stories-container.component.html',
  styleUrls: ['./stories-container.component.scss']
})
export class StoriesContainerComponent implements OnInit {

  /**
   * children template reference
   */
  @Input() children: TemplateRef<any>;

  /**
   * background color
   */
  @Input() bgColor = 'white';

  constructor() { }

  /**
   * lifecycle hook indicate that Angular is done creating the component
   */
  ngOnInit() {
  }

}
