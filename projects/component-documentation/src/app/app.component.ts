import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'component-documentation';
  statusOptions = [
    {
      label: 'All',
      value: { id: 0, name: 'All', code: 'All' },
    },
    {
      label: 'Sent',
      value: { id: 1, name: 'Sent', code: 'Sent' },
    },
    {
      label: 'Scheduled',
      value: { id: 2, name: 'Scheduled', code: 'Scheduled' },
    },
    {
      label: 'Draft',
      value: { id: 3, name: 'Draft', code: 'Draft' },
    },
  ];
  /** default value of sortByDropDown */
  defaultValue: any
  constructor(){
    /** default value for sortby dropdown. */
    this.defaultValue = this.statusOptions[0]
  }
}
