import {moduleMetadata, storiesOf} from '@storybook/angular';
import {ComponentLibraryModule} from '@relatient/component-library';
import {StoriesContainerComponent} from '../../../app/components/containers/stories-container/stories-container.component';
import markdown from './button.stories.md';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

storiesOf('Common Components/Button', module)
  .addDecorator(
    moduleMetadata({
      declarations: [StoriesContainerComponent],
      imports: [ComponentLibraryModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
  )
  .addParameters({
    notes: markdown
  })
  .add('primary', () => ({
    template: `
      <app-stories-container [children]="children">
        <ng-template #children>
          <app-button [btnText]="label" (buttonClick)="handleClick($event)"></app-button>
        </ng-template>
      </app-stories-container>`,
    props: {
      label: 'Save',
      handleClick: event => {
        console.log('some bindings work');
        console.log(event);
      }
    }
  }))
  .add('secondary', () => ({
    template: `
      <app-stories-container [children]="children">
        <ng-template #children>
          <app-button [btnText]="label" theme="ternary" (buttonClick)="handleClick($event)"></app-button>
        </ng-template>
      </app-stories-container>`,
    props: {
      label: 'Save',
      handleClick: event => {
        console.log('some bindings work');
        console.log(event);
      }
    }
  }))
  .add('disabled', () => ({
    template: `
      <app-stories-container [children]="children">
        <ng-template #children>
          <app-button [btnText]="label" (buttonClick)="handleClick($event)" [disabled]="disabled"></app-button>
        </ng-template>
      </app-stories-container>`,
    props: {
      label: 'Save',
      handleClick: event => {
        console.log('some bindings work');
        console.log(event);
      },
      disabled: true
    }
  }))
  .add('loading', () => ({
    template: `
      <app-stories-container [children]="children">
        <ng-template #children>
          <app-button btnText="Save" loadingText="Saving..." iconClass="icon-loader" isLoading="true"></app-button>
        </ng-template>
      </app-stories-container>`,
    props: {
      label: 'Click me!',
      handleClick: event => {
        console.log('some bindings work');
        console.log(event);
      },
      loading: true,
      disabled: false
    }
  }));

