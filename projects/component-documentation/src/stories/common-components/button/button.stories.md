# Button component documentation

### Browser Rendered
- 44px height
- 14px top and bottom
- 16px left and right
- border radius: 4px;
- foreground color: #FFFFFF
- background color: #0075A3
- hover background color: #00658C

### HTML Code
```html
<button
  (click)="onClick($event)"
  [class]="classList"
  [class.disabled]="disabled"
  [disabled]="disabled"
>
  <span class="text">{{ label }}</span>
</button>

```

### SCSS Code
```scss
@import '../../../styles/base/variables';
@import '../../../styles/utilities/extends';

.button-component {
  @extend %primary-cta;

  &.disabled {
    cursor: not-allowed;
    opacity: 0.6;
  }
}
```

### Typescript Code 
```typescript
import {Component, EventEmitter, HostBinding, Input, OnInit, Output, } from '@angular/core';

/**
 * Button component
 */
@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent implements OnInit {
  /**
   * button's class
   */
  @Input()
  class: string;
  /**
   * button's label
   */
  @Input()
  label: string;
  /**
   * disable button if the this property is true
   */
  @Input()
  disabled: false;
  /**
   * class
   */
  @HostBinding('class')
  classList = 'button-component';
  /**
   * emit event when click
   */
  @Output()
  handleClick = new EventEmitter<any>();

  constructor() {
  }

  /**
   * initial
   */
  ngOnInit() {
    if (this.class) {
      this.classList += ' ' + this.class;
    }
  }

  /**
   * click function
   * @param event
   */
  onClick(event) {
    this.handleClick.emit(event);
  }
}
```

### Unit test cases
```typescript
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ButtonComponent} from './button.component';

describe('ButtonComponent', () => {
  let component: ButtonComponent;
  let fixture: ComponentFixture<ButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ButtonComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

```
